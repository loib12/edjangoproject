import React, {Component} from 'react'
import logo from '../images/icons/logo.png'
import icon_header_01 from '../images/icons/icon-header-01.png'
import icon_header_02 from '../images/icons/icon-header-02.png'
import item_cart_01 from '../images/item-cart-01.jpg'

export default class Header extends Component {
    constructor(props) {
        super(props)

        this.state = {
            listCategories : [],
            Item:{
            id: null,
            name: '',
            slug: '',
            status: false
          }
        }

        this.fetchCategories = this.fetchCategories.bind(this);
    }

    // lifecycle method
    componentWillMount() {
        this.fetchCategories()
  }

    fetchCategories(){

    fetch('http://127.0.0.1:8000/api/category-list').then(response => response.json()) // convert sang json
        .then(data =>
            this.setState({
               listCategories:data
            })
        )
  }

    render() {
        var categories = this.state.listCategories
        return(
            <div>
                <header className="header1">
                    <div className="container-menu-header">
                        <div className="topbar">
                            <div className="topbar-social">
                                <a href="#" className="topbar-social-item fa fa-facebook"></a>
                                <a href="#" className="topbar-social-item fa fa-instagram"></a>
                                <a href="#" className="topbar-social-item fa fa-pinterest-p"></a>
                                <a href="#" className="topbar-social-item fa fa-snapchat-ghost"></a>
                                <a href="#" className="topbar-social-item fa fa-youtube-play"></a>
                            </div>

                            <span className="topbar-child1">
					            Free shipping for standard order over $100
				            </span>

                            <div className="topbar-child2">
                                <span className="topbar-email">
                                    fashe@example.com
                                </span>

                                <div className="topbar-language rs1-select2">
                                    <select className="selection-1" name="time">
                                        <option>USD</option>
                                        <option>EUR</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div className="wrap_header">
                            <a href="index.html" className="logo">
                                <img src={logo} alt="IMG-LOGO"/>
                            </a>

                            <div className="wrap_menu">
                                <nav className="menu">
                                    <ul className="main_menu">
                                        <li>
                                            <a href="index.html">Home</a>

                                        </li>

                                        <li>
                                            <a href="product.html">Category</a>
                                            <ul className="sub_menu">
                                                {categories.map((c, index) =>
                                                    <li key={index}><a href="index.html">{c.name}</a></li>
                                                )}


                                            </ul>
                                        </li>

                                        <li>
                                            <a href="product.html">Shop</a>
                                        </li>

                                        <li className="sale-noti">
                                            <a href="product.html">Sale</a>
                                        </li>

                                        <li>
                                            <a href="cart.html">Features</a>
                                        </li>

                                        <li>
                                            <a href="blog.html">Blog</a>
                                        </li>

                                        <li>
                                            <a href="about.html">About</a>
                                        </li>

                                        <li>
                                            <a href="contact.html">Contact</a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>

                            <div className="header-icons">
                                <a href="#" className="header-wrapicon1 dis-block">
                                    <img src={icon_header_01} className="header-icon1" alt="ICON"/>
                                </a>

                                <span className="linedivide1"></span>

                                <div className="header-wrapicon2">
                                    <img src={icon_header_02}
                                         className="header-icon1 js-show-header-dropdown" alt="ICON"/>
                                        <span className="header-icons-noti">0</span>

                                        <div className="header-cart header-dropdown">
                                            <ul className="header-cart-wrapitem">
                                                <li className="header-cart-item">
                                                    <div className="header-cart-item-img">
                                                        <img src={item_cart_01} alt="IMG"/>
                                                    </div>

                                                    <div className="header-cart-item-txt">
                                                        <a href="#" className="header-cart-item-name">
                                                            White Shirt With Pleat Detail Back
                                                        </a>

                                                        <span className="header-cart-item-info">
											                1 x $19.00
										                </span>
                                                    </div>
                                                </li>

                                            </ul>

                                            <div className="header-cart-total">
                                                Total: $75.00
                                            </div>

                                            <div className="header-cart-buttons">
                                                <div className="header-cart-wrapbtn">

                                                    <a href="cart.html"
                                                       className="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
                                                        View Cart
                                                    </a>
                                                </div>

                                                <div className="header-cart-wrapbtn">

                                                    <a href="#"
                                                       className="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
                                                        Check Out
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    {/*Header Mobile*/}
                    <div className="wrap_header_mobile">
                        <a href="index.html" className="logo-mobile">
                            <img src={logo} alt="IMG-LOGO"/>
                        </a>

                        <div className="btn-show-menu">
                            <div className="header-icons-mobile">
                                <a href="#" className="header-wrapicon1 dis-block">
                                    <img src={icon_header_01} className="header-icon1" alt="ICON"/>
                                </a>

                                <span className="linedivide2"></span>

                                <div className="header-wrapicon2">
                                    <img src={icon_header_02}
                                         className="header-icon1 js-show-header-dropdown" alt="ICON"/>
                                        <span className="header-icons-noti">0</span>

                                        <div className="header-cart header-dropdown">
                                            <ul className="header-cart-wrapitem">
                                                <li className="header-cart-item">
                                                    <div className="header-cart-item-img">
                                                        <img src={item_cart_01} alt="IMG"/>
                                                    </div>

                                                    <div className="header-cart-item-txt">
                                                        <a href="#" className="header-cart-item-name">
                                                            White Shirt With Pleat Detail Back
                                                        </a>

                                                        <span className="header-cart-item-info">
                                                            1 x $19.00
                                                        </span>
                                                    </div>
                                                </li>
                                            </ul>

                                            <div className="header-cart-total">
                                                Total: $75.00
                                            </div>

                                            <div className="header-cart-buttons">
                                                <div className="header-cart-wrapbtn">
                                                    <a href="cart.html"
                                                       className="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
                                                        View Cart
                                                    </a>
                                                </div>

                                                <div className="header-cart-wrapbtn">
                                                    <a href="#"
                                                       className="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
                                                        Check Out
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </div>

                            <div className="btn-show-menu-mobile hamburger hamburger--squeeze">
                                <span className="hamburger-box">
                                    <span className="hamburger-inner"></span>
                                </span>
                            </div>
                        </div>
                    </div>

                </header>
            </div>
        )
    }
}