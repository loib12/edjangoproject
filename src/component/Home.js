import React, {Component} from 'react'
import Slide from './Slide'
import Banner from './Banner'
import NewProduct from './NewProduct'

export default class Home extends Component {
    constructor(props) {
        super(props)
    }


    render() {
        return (
            <div>
                <Slide/>
                <Banner/>
                <NewProduct/>
            </div>


        )
    }
}