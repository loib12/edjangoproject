import React, {Component} from 'react'
import {  Link } from 'react-router-dom';

export default class NewProduct extends Component {
    constructor(props) {
        super(props)

        this.state = {
            listProducts: []
        }

        this.fetchProducts = this.fetchProducts.bind(this)
    }

    componentWillMount() {
        this.fetchProducts()
    }

    fetchProducts() {
        fetch('http://127.0.0.1:8000/api/product-list').then(response => response.json())
            .then(data =>
                this.setState({
                    listProducts: data
                })
            )
    }


    render(){
        var products = this.state.listProducts;
        console.log(products);
        return (

                <section className="blog bgwhite p-t-94 p-b-65">
                    <div className="container">
                        <div className="sec-title p-b-52">
                            <h3 className="m-text5 t-center">
                                Our New Product
                            </h3>
                        </div>

                        <div className="row">
                            {products.map((p, index) =>
                                <div className="col-sm-10 col-md-4 p-b-30 m-l-r-auto" key={index}>
                                    <div className="block3">
                                        <a href="blog-detail.html" className="block3-img dis-block hov-img-zoom">
                                            <img src={p.image} alt="IMG-BLOG" />
                                        </a>

                                        <div className="block3-txt p-t-14">
                                            <h4 className="p-b-7">

                                                <Link to={`/detail/${p.id}`} className="m-text11"><p>{p.name}</p></Link>

                                            </h4>

                                            <span className="s-text6">Price:</span> <span className="s-text7">{p.price} $</span>

                                            <p className="s-text8 p-t-16">
                                                {p.description}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            )}

                        </div>
                    </div>
                </section>


        )
    }
}