import React, {Component} from 'react'


export default class Product extends Component {
    constructor(props) {
        super(props)
        this.state = {
            listProducts: [],
            Item: {
                id : null,
                name: '',
                slug: '',
                price: 0,
                sale_price: 0,
                content: '',
                image: '',
                status: false,
                category: 1
            }
        }
    this.fetchProducts = this.fetchProducts.bind(this)
    }

    componentWillMount() {
        this.fetchProducts()
    }

    fetchProducts(){

    fetch('http://127.0.0.1:8000/api/product-list').then(response => response.json()) // convert sang json
        .then(data =>
            this.setState({
               listProducts:data
            })
        );
  }

    render() {
        var products = this.state.listProducts
        return(
            <table className="table">

            <thead>
              <tr>
                <th>id</th>
                <th>name</th>
                <th>slug</th>
                <th>price</th>
                <th>sale_price</th>
                <th>content</th>
                <th>image</th>
                <th>status</th>
                <th>category</th>
              </tr>
            </thead>

            <tbody>
            {products.map((p, index) =>
              <tr key={index}>
                  <td>{p.id}</td>
                  <td>{p.name}</td>
                  <td>{p.slug}</td>
                  <td>{p.price}</td>
                  <td>{p.sale_price}</td>
                  <td>{p.content}</td>
                  <td width="20px"><img src={p.image} alt=""/></td>
                  <td>{p.status ? "display": "hide"}</td>
                  <td>{p.category}</td>

              </tr>
            )}

            </tbody>

          </table>
        )
    }
}