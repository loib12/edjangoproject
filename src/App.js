import React from 'react'
import Header from './component/Header'
import Shipping from './component/Shipping'
import Footer from './component/Footer'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import DetailProduct from "./component/DetailProduct";
import Home from './component/Home'

class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      listCategories:[],
      Item:{
        id: null,
        name: '',
        slug: '',
        status: false
      }
    }
    this.fetchCategories = this.fetchCategories.bind(this);
  }

  // lifecycle method
  componentWillMount() {
    this.fetchCategories()
  }

  fetchCategories(){

    fetch('http://127.0.0.1:8000/api/category-list').then(response => response.json()) // convert sang json
        .then(data =>
            this.setState({
               listCategories:data
            })
        )
  }

  render() {
    var categories = this.state.listCategories
    return (
        <Router>
            <div>
                <Header/>
                <Switch>
                    <Route path="/" exact component={Home}/>
                    <Route path="/detail/:id" component={DetailProduct}/>
                </Switch>
                <Shipping/>
                <Footer/>

            </div>
        </Router>

    )
  }
}


export default App;
